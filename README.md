# jekyll-radius

Radius, A fully responsive masonry-style portfolio template.


## Installation

Add this line to your Jekyll site's Gemfile:

```ruby
gem "jekyll-radius"
```

And add this line to your Jekyll site:

```yaml
theme: jekyll-radius
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-radius

## Usage

To add image create `post-name.md` file under `_posts` folder having following front matter.

```yaml
	---
	layout: post
	title: Lorem Ipsum
	style: vertical
	image: image_path
	---

	Content of post here.
```
Edit `_config.yml` file for setting up site name and other configurable parameters.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/hello. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, run `bundle install`.

You theme is setup just like a normal Jelyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

